# Pico y Plca Predictor
Es una aplicacion sencilla para predecir pico y placa segun la fecha la hora y el ultimo numero de placa consta de test y version minificada con webpack

![screenshot](./pico.png)



## Hecho con

- JavaScript
- Jest
- Webpack

## Información
- ./dist Version minificada de webpack
- ./src  Logica
- ./__tests_ Pruebas


## Live Demo
[Live Demo Link](https://pico-y-placa-predictor-alexzambra2610-c3600651b5739c66232f38d08.gitlab.io/)


### Test
- `npm run test`

### Prerequisitos

Buscador
Internet



## Autor

👤 **Pablo Alexis Zambrano Coral**
- Github: [@Alexoid1](https://github.com/Alexoid1)
- Twitter: [@Alexis Zambrano_acz](https://twitter.com/pablo_acz)
- Linkedin: [linkedin](https://www.linkedin.com/in/pablo-alexis-zambrano-coral-7a614a189/)

## 🤝 Contribuir

Las Contribuciones son Bienvenidas!

Checa --> [issues page](https://gitlab.com/alexzambra2610/pico-y-placa-predictor).


## 📝 Licensia

This project is [MIT]() licensed.
